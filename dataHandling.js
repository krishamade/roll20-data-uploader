const fs = require("fs").promises;
const moment = require("moment");
const path = require("path");
const Roll20Data = require("./models/roll20Data");  // Path to your Roll20Data model
require("dotenv").config();

// Function to create filename with specific format
const createFileName = (type) => {
  const date = moment().format("YYYYMMDD");
  return path.join(__dirname, "data", `${date}-${type}Export.json`);
};

// Function to delete older files with the same type
const deleteOldFiles = async (type) => {
  const files = await fs.readdir(path.join(__dirname, "data"));
  const filteredFiles = files.filter((file) => file.endsWith(`${type}Export.json`));

  for (const file of filteredFiles) {
    await fs.unlink(path.join(__dirname, "data", file));
  }
};

// Function to export data to a JSON file
const exportData = async (data, type) => {
  const filename = createFileName(type);

  const formattedData = data.map((item) => ({
    Name: item.name,
    Bio: item.bio,
  }));

  try {
    await deleteOldFiles(type);
    await fs.writeFile(filename, JSON.stringify(formattedData, null, 2));
    console.log(`Data has been written to file ${filename}`);
  } catch (err) {
    throw err;
  }
};

async function sendData(type, characters) {
  console.log(`sendData: Starting sendData for type: ${type}`);
  try {
    // Delete all existing documents of this type
    await Roll20Data.deleteMany({ Type: type });
    console.log(`All existing documents of type ${type} have been deleted.`);

    // Prepare data for insertion
    let data = characters.map(item => ({
      Name: item.name.replace(/\u00A0/g, ' '),
      Bio: item.bio.replace(/\u00A0/g, ' '),
      Type: type
    }));

    // Insert new documents
    await Roll20Data.insertMany(data);
    console.log(`${data.length} documents were added.`);

    return { addedCount: data.length };
  } catch (err) {
    console.error('Error during sendData operation:', err);
    throw err;
  }
}



module.exports = {
  exportData,
  sendData,
};
