const express = require('express');
const multer = require('multer');
const path = require('path');
const { sendData } = require('./dataHandling');
const { connectDB } = require('./db');
require('dotenv').config();
const fs = require('fs').promises;

const app = express();
const upload = multer({ dest: 'uploads/' });

// Connect to MongoDB
connectDB().then(() => {
    console.log('MongoDB Connected');
}).catch((err) => {
    console.error('MongoDB connection error:', err);
    process.exit(1); // Exit the app if the database connection fails
});

app.use(express.static('public')); // Serve static files from the public directory

app.post('/upload', upload.single('file'), async (req, res) => {
    try {
        const filePath = path.join(__dirname, req.file.path);
        console.log(`File uploaded to: ${filePath}`);

        // Read and parse the file
        const fileContent = await fs.readFile(filePath, 'utf8');
        const characters = JSON.parse(fileContent);

        // Call sendData function with the parsed data
        const { addedCount, updatedCount } = await sendData('characters', characters);

        res.status(200).json({ message: 'File processed', addedCount, updatedCount });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Error processing file' });
    }
});

const PORT = process.env.PORT || 3012;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
