FROM node:slim

#Set Docker Container Timezone
ENV TZ=America/Detroit

#Set Environment Variables
ARG MONGODB_URI=default_value
ENV MONGODB_URI=${MONGODB_URI}

#Create App Directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

#Install app dependencies
COPY package*.json ./
RUN npm install

# Bundle app source
COPY . .

# Copy .env file
COPY .env ./

CMD ["node", "server.js"]
EXPOSE 8912
